import React, { Component } from 'react'
import '../_core/gallery.scss';
export default class Images extends Component {
  render() {
    return (
      <div>
        <section className="gallery">
  <h1>YOUR GALLERY</h1>
  <div className="gallery__content">
    <div className="gallery__items  gallery__items1">
      <a href="/img/g1.jpg" data-lightbox="gallery1">
        <img src="/img/g1.jpg" alt="" />
        <div className="gallery__detail">
          <h3>OUR GALLERY</h3>
          <p className="heart">Lorem ipsum dolor sit amet consectetur!</p>
        </div>
      </a>
    </div>
    <div className="gallery__items">
      <a href="/img/g2.jpg" data-lightbox="gallery1">
        <img src="/img/g2.jpg" alt="" />
        <div className="gallery__detail">
          <h3>OUR GALLERY</h3>
          <p>Lorem ipsum dolor sit amet consectetur!</p>
        </div>
      </a>
    </div>
    <div className="gallery__items">
      <a href="/img/g3.jpg" data-lightbox="gallery1">
        <img src="/img/g3.jpg" alt="" />
        <div className="gallery__detail">
          <h3>OUR GALLERY</h3>
          <p>Lorem ipsum dolor sit amet consectetur!</p>
        </div>
      </a>
    </div>
    <div className="gallery__items">
      <a href="/img/g4.jpg" data-lightbox="gallery1">
        <img src="/img/g4.jpg" alt="" />
        <div className="gallery__detail">
          <h3>OUR GALLERY</h3>
          <p>Lorem ipsum dolor sit amet consectetur!</p>
        </div>
      </a>
    </div>
    <div className="gallery__items">
      <a href="/img/g5.jpg" data-lightbox="gallery1">
        <img src="/img/g5.jpg" alt="" />
        <div className="gallery__detail">
          <h3>OUR GALLERY</h3>
          <p>Lorem ipsum dolor sit amet consectetur!</p>
        </div>
      </a>
    </div>
    <div className="gallery__items gallery__items2">
      <a href="/img/g6.jpg" data-lightbox="gallery1">
        <img src="/img/g6.jpg" alt="" />
        <div className="gallery__detail">
          <h3>OUR GALLERY</h3>
          <p>Lorem ipsum dolor sit amet consectetur!</p>
        </div>
      </a>
    </div>
  </div>
</section>

      </div>
    )
  }
}
