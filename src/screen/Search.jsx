import React, { Component } from "react";
import { fetchGif } from "../redux/UserAction";
import "../_core/search.scss";
import { connect } from "react-redux";

class Search extends Component {
  // Constructor
  constructor(props) {
    super(props);
    this.state = {
      //keyWord: "",
      allGifArr: [],
      user: [],
      //resultSearch: []
    };
  }

  // Handle Logic

  onClickSearch = () => {
    let allGif = this.state.allGifArr;

    this.props.dispatch(fetchGif(allGif));
  };

  //render
  render() {
    // let url = `https://api.giphy.com/v1/gifs/search?api_key=2FSIrWx8A55dvOj28YTYBIB5YOlB1lgJ&q=1&limit=25&offset=0&rating=g&lang=en`

    return (
      <div>
        <div className="input-group header-txt-search container">
          <input
            type="text"
            className="form-control"
            placeholder="Start searching for images"
            style={{ maxWidth: "980px" }}
          />
          <div className="input-group-prepend">
            <button
              className="input-group-text bg-white"
              id="btnSearch"
              name="search"
              onClick={this.onClickSearch}

              // value={this.state.keyWord}
            >
              <i className="fa fa-search"></i>Search
            </button>
          </div>
          <div className="input-group-prepend">
            <button className="input-group-text bg-white">
              <i className="fab fa-gratipay"></i>Favourites
            </button>
          </div>
        </div>
        <div className="wrapper">
          <div className="row">
            <div className=" item item1 activeImg">
              <img src="/img/tn5.jpg" alt="img"></img>
              {/* <svg className="heart" viewBox="0 0 32 29.6">
  <path d="M23.6,0c-3.4,0-6.3,2.7-7.6,5.6C14.7,2.7,11.8,0,8.4,0C3.8,0,0,3.8,0,8.4c0,9.4,9.5,11.9,16,21.2
	c6.1-9.3,16-12.1,16-21.2C32,3.8,28.2,0,23.6,0z" />
</svg> */}

            </div>
            <div className=" item item2 activeImg">
              <img src="/img/tn5.jpg" alt="img"></img>
              {/* <section className="ac-footer">
          <br />
          <center><p>hover đi</p></center>

          <div className="ac-footer-container ac-footer-brand">
            <span className="ac-icon ac-icon-love-dark" />
          </div>
        </section> */}
      
            </div>
            <div className=" item item3 activeImg">
              <img src="/img/tn5.jpg" alt="img"></img>
            </div>
            <div className="item item4 activeImg">
              <img src="/img/tn5.jpg" alt="img"></img>
            </div>
          </div>
          <div className="row">
            <div className="item item5 activeImg">
              <img src="/img/tn5.jpg" alt="img"></img>
            </div>
            <div className="item item6 activeImg">
              <img src="/img/tn5.jpg" alt="img"></img>
            </div>
            <div className="item item7 activeImg">
              <img src="/img/tn5.jpg" alt="img"></img>
            </div>
            <div className="item item8 activeImg">
              <img src="/img/tn5.jpg" alt="img"></img>
            </div>
            <div>
              {/* <svg class="heart" viewBox="0 0 32 29.6">
  <path d="M23.6,0c-3.4,0-6.3,2.7-7.6,5.6C14.7,2.7,11.8,0,8.4,0C3.8,0,0,3.8,0,8.4c0,9.4,9.5,11.9,16,21.2
	c6.1-9.3,16-12.1,16-21.2C32,3.8,28.2,0,23.6,0z"/>
</svg>  */}
            </div>
          </div>
        </div>
        <div>{this.renderAllGif()}</div>
        
      </div>
    );
  }
  renderAllGif = () => {
    let arr = this.props.ALL_GIF.user;
    console.log("Search -> allGifArr", arr);
    if (this.state.allGifArr.length > 0) {
      arr = this.state.user;
      //console.log("Search -> renderAllGif -> this.state.user", this.state.user)
    }
    // console.log(' đây là this.props.arr',this.props.arr);
    // this.props.ar&& sau dấu=
    //console.log("Search -> this.props.arr.map", this.props);
    // let allGif = this.props.arr.user;
    // console.log("Search -> this.props.arr", this.props.arr);

    //  let allGif = arr.ALL_GIF.data((item, index) => {

    //   return (

    //     <div className="row" key={index}>
    //       <div className="col-2">{item}</div>
    //     {/* <div className="col-2">{item.user.data[0].images}</div> */}
    //     {/* <div className="col-2">{item.hinh2}</div>
    //     <div className="col-2">{item.hinh3}</div>
    //     <div className="col-2">{item.hinh4}</div>

    //     <div className="col-2">{item.hinh5}</div>
    //     <div className="col-2">{item.hinh6}</div>
    //     <div className="col-2">{item.hinh7}</div>
    //     <div className="col-2">{item.hinh8}</div> */}
    //     </div>

    //   );
    // });

    // return (
    //   <div className="container">
    //     <div className="row">{allGif}</div>
    //   </div>
    // );
  };

  // componentDidMount() {
  //   console.log("Search -> componentDidMount -> this.props", this.props.get())
  //   this.props.get();
  // }
}
// Redux
// const mapDispatchToProps = dispatch => {
//   return {
//     get: () => {
//       dispatch(fetchGif());
//     },

//     // add: () => {
//     //   dispatch(addNewImg());
//     // }
//   };
// };

const mapStateToProps = (state) => {
  return {
    ALL_GIF: state.user || [],
  };
};

export default connect(mapStateToProps)(Search);
