import axios from "axios";
import { settings } from "../redux/setting";

 const restConnector = axios.create({
 
  baseURL: settings.domain
});
export default restConnector;